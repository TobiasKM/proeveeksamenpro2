public class MainApp {

    public static void main(String[] args) {

        BinaryTree<String> b1 = new BinaryTree<String>("2");
        BinaryTree<String> b2 = new BinaryTree<String>("4");

        BinaryTree<String> b3 = new BinaryTree<String>("+", b1, b2);
        BinaryTree<String> b4 = new BinaryTree<String>("7");

        BinaryTree<String> b5 = new BinaryTree<String>("3");
        BinaryTree<String> b6 = new BinaryTree<String>("8");

        BinaryTree<String> b7 = new BinaryTree<String>("*", b3, b4);
        BinaryTree<String> b8 = new BinaryTree<String>("+", b5, b6);

        BinaryTree<String> b9 = new BinaryTree<String>("+", b7, b8);

        //System.out.println(b9.countElements("+"));

        //System.out.println(b9.value());

       b9.print();

    }
}
